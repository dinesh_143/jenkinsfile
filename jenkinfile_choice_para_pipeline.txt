pipeline {
    agent any
    stages {
        stage('parameters_class') {
            steps {
                script {
                    properties ([
                        parameters ([
                            choice(
                                choices: ['APPLE_BUILD', 'BAT_BUILD'],
                                name: 'PARAMETER_NAME'
                            ),
                        ])
                    ])
                }
            }
        }
    
    
        stage('test3') {
            steps {
                script {
                    if (env.BRANCH_NAME == 'master') {
                        echo 'I only execute on the master branch'
                    } else {
                        echo 'I execute elsewhere'
                    }
                }
            }
        }

    }
}